+++
title = "PVE"
weight = 1
+++

* create a dedicated pool:

```bash
pveum pool add cloud-init
```

* create a dedicated user:

```bash
PASSWORD=$(openssl rand -hex 12); pveum user add cloudinit@pve --password $PASSWORD; echo $PASSWORD
```

* create a dedicated storage for vms, e.g. using zfs (other types like btrfs | cephfs | cifs | dir | glusterfs | iscsi | iscsidirect | lvm | lvmthin | nfs | pbs | rbd | zfs | zfspool will work too)

```bash
zfs create -p rpool/vms/cloud-init
pvesh create storage --type zfspool --pool rpool/vms/cloud-init --storage cloud-init --content images --sparse 1
```

* add user permissions:
  * PVEVMAdmin for the pool
  * PVEDatastoreAdmin for the vm storage
  * PVEDatastoreUser for the snippet storage created in previous step

```bash
pveum acl modify /pool/cloud-init/ --users cloudinit@pve --role PVEVMAdmin
pveum acl modify /storage/cloud-init --users cloudinit@pve --role PVEDatastoreAdmin
pveum acl modify /storage/snippets --users cloudinit@pve --role PVEDatastoreUser
```

On each cluster node, you need to provide cloud-init capable images like [Ubuntu Minimal](https://cloud-images.ubuntu.com/minimal/daily/jammy/current/jammy-minimal-cloudimg-amd64.img).

It is advised to store this on shared storage, but local storage will work too (Just needs to be done on every node).

## Recommended: automatic sync

See [Image sync]({{< relref "/usage/pvecic-image-sync/" >}})

## Example: Manually

{{% notice note %}}
Remember to do this on every cluster node
{{% /notice %}}

```bash
curl -LO /mnt/pve/shared-nfs-storage/template/iso/jammy-minimal-cloudimg-amd64.img https://cloud-images.ubuntu.com/minimal/daily/jammy/current/jammy-minimal-cloudimg-amd64.img
```
