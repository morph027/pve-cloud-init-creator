set -e

if ! groups pvecic | grep -q ssl-cert; then
    gpasswd -a pvecic ssl-cert
fi

if [ ! -d /etc/pvecic-snippets-endpoint ]; then
    install \
        -o pvecic \
        -g pvecic \
        -d \
        /etc/pvecic-snippets-endpoint
fi
