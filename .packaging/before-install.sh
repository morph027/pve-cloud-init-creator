set -e

getent passwd pvecic >/dev/null 2>&1 || adduser \
    --system \
    --shell /usr/sbin/nologin \
    --gecos "PVE Cloud-Init Creator" \
    --group \
    --disabled-password \
    --home /var/lib/pvecic \
    pvecic

if [ ! -f /var/lib/pvecic/.config/rclone/rclone.conf ]; then
    install \
        -o pvecic \
        -g pvecic \
        -d \
        /var/lib/pvecic/.config
    install \
        -o pvecic \
        -g pvecic \
        -d \
        /var/lib/pvecic/.config/rclone
    install \
        -o pvecic \
        -g pvecic \
        -m 640 \
        /dev/null \
        /var/lib/pvecic/.config/rclone/rclone.conf
fi
