+++
title = "Image mirror"
weight = 4
+++

{{% notice note %}}
Optional
{{% /notice %}}

The image mirror will just serve the synced image mirror using http. This allows us to use some nasty rclone hacks like mounting the http mirror into the filesystem in clusters without shared storage.

## Configuration values

Options can be set in `/etc/default/pvecic-image-mirror`. If empty or does not exist, the default values from `/usr/share/pvecic/image-mirror.env` will be applied.

| Value | Comment |
|-|-|
| **`DIR`** | directory to serve cloud images from - should be the same like for [Image sync]({{< relref "/usage/pvecic-image-sync/" >}}) |
| **`PARAMS`** | other [rclone serve http](https://rclone.org/commands/rclone_serve_http/) parameters |

## http mount

If you run sync and mirror on any machine in your network or cluster and your cluster does not have shared storage, you can just mount the http mirror using rclone.

* setup rclone as [mount helper](https://rclone.org/commands/rclone_mount/#rclone-as-unix-mount-helper):

```bash
ln -s /usr/bin/rclone /sbin/mount.rclone
```

* add mount to fstab:

```bash
:http: /mnt/cloud-images.ubuntu.com rclone http_url=http://your-cloud-image-mirror:8080/,ro,_netdev,config=/tmp/rclone.conf 0 0
```
