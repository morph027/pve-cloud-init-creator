#!/bin/bash

set -e

tmpdir="$(mktemp -d)"
export version=${CI_COMMIT_TAG:-0.0.0+${CI_COMMIT_SHORT_SHA}}
/root/.pyenv/shims/pip3 install -q -r "${CI_PROJECT_DIR}"/requirements.txt
echo "__version__ = '${version}'" >_version.py
/root/.pyenv/shims/python3 \
    "${CI_PROJECT_DIR}"/pve_cloud_init_creator.py \
    --print-completion bash \
    >"${tmpdir}"/pve-cloud-init-creator-bash-completion
cd "${tmpdir}"
/root/.pyenv/shims/nuitka \
    --onefile \
    --include-package=proxmoxer \
    --remove-output \
    -o \
    pve-cloud-init-creator \
    "${CI_PROJECT_DIR}"/pve_cloud_init_creator.py
./pve-cloud-init-creator --help
cd -
echo "${CI_JOB_ID}" >"${CI_PROJECT_DIR}"/CI_JOB_ID
eval "$(dpkg-architecture -s)"
if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.39.0}"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${DEB_HOST_ARCH}.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi
cat >"${tmpdir}/nfpm.yaml" <<EOF
arch: ${DEB_HOST_ARCH}
name: ${NAME}
version: ${version}
version_schema: none
license: MIT
description: ${DESCRIPTION}
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
homepage: https://gitlab.com/morph027/pve-cloud-init-creator/
contents:
  - src: ${tmpdir}/pve-cloud-init-creator-bash-completion
    dst: /etc/bash_completion.d/pve-cloud-init-creator
  - src: ${tmpdir}/pve-cloud-init-creator
    dst: /usr/bin/pve-cloud-init-creator
    file_info:
      mode: 0755
EOF
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
apt-get -y install ./"${NAME}"*.deb
