#!/usr/bin/env python3
"""create cloud-init based virtual machines on proxmox ve"""


import io
import logging
import time
from sys import exit as sys_exit
from urllib.parse import quote
from requests.exceptions import ConnectionError as RequestsConnectionError

import coloredlogs
import configargparse
import requests
import shtab
import verboselogs
from jsonschema import ValidationError, validate
from munch import DefaultMunch
from proxmoxer import ProxmoxAPI, ResourceException
from pyaml_env import parse_config
from yaml import safe_dump as yaml_safe_dump

from _version import __version__

DEFAULT_USER_DATA = {
    "manage_etc_hosts": True,
    "chpasswd": {"expire": False},
    "users": ["default"],
    "package_upgrade": True,
}

CONFIG_SCHEMA = {
    "type": "object",
    "required": ["pve"],
    "properties": {
        "pve": {
            "type": "object",
            "additionalProperties": False,
            "required": [
                "host",
                "port",
                "user",
                "pool",
                "node",
                "storage",
            ],
            "properties": {
                "host": {"type": "string"},
                "port": {"type": "integer"},
                "user": {
                    "type": "string",
                    "pattern": "^.*@.*",
                },
                "password": {"type": "string"},
                "token_name": {"type": "string"},
                "token_value": {"type": "string"},
                "pool": {"type": "string"},
                "node": {"type": "string"},
                "storage": {"type": "string"},
                "snippets": {"type": "string"},
                "verify_ssl": {"type": "boolean"},
            },
            "oneOf": [
                {"required": ["password"]},
                {"required": ["token_name", "token_value"]},
            ],
            "dependencies": {
                "password": {"not": {"required": ["token_name", "token_value"]}},
                "token_name": {"not": {"required": ["password"]}},
                "token_value": {"not": {"required": ["password"]}},
            },
        },
        "snippets": {
            "type": "object",
            "additionalProperties": False,
            "properties": {
                "endpoint": {
                    "type": "string",
                    "pattern": "^https://.*:.*@.*",
                },
                "verify_ssl": {"type": "boolean"},
                "storage": {"type": "string"},
            },
        },
        "vm": {
            "type": "object",
            "additionalProperties": False,
            "properties": {
                "": {"type": "boolean"},
                "ssh_key": {"type": "string"},
                "userdata": {"type": "string"},
                "image": {"type": "string"},
                "netconfig": {"type": "string"},
                "ipconfig": {"type": "string"},
                "ostype": {"type": "string"},
                "scsihw": {"type": "string"},
                "storage": {
                    "type": "object",
                    "additionalProperties": False,
                    "required": ["size"],
                    "properties": {
                        "size": {"type": "string"},
                    },
                },
                "cpu": {
                    "type": "object",
                    "additionalProperties": False,
                    "required": ["type"],
                    "properties": {
                        "type": {"type": "string"},
                        "cores": {"type": "integer"},
                    },
                },
                "memory": {
                    "type": "object",
                    "additionalProperties": False,
                    "required": ["size"],
                    "properties": {
                        "size": {"type": "integer"},
                    },
                },
            },
            "oneOf": [
                {"required": ["ssh_key"]},
                {"required": ["userdata"]},
            ],
            "dependencies": {
                "ssh_key": {"not": {"required": ["userdata"]}},
                "userdata": {"not": {"required": ["ssh_key"]}},
            },
        },
    },
}


def clean_dict(data: dict) -> dict:
    """
    Remove only '' and empty dictionary
    https://stackoverflow.com/a/73207387
    """
    if not isinstance(data, dict):
        return data
    return {
        key: _value
        for key, value in data.items()
        if (_value := clean_dict(value)) not in ("", {})
    }


def get_logger(name: str, arguments: configargparse.Namespace):
    """get and configure logger"""
    verboselogs.install()
    this = logging.getLogger(name)
    no_color_bold = {"bold": True, "color": ""}
    field_styles = coloredlogs.DEFAULT_FIELD_STYLES
    field_styles.update(
        {
            "levelname": no_color_bold,
            "programname": no_color_bold,
            "hostname": no_color_bold,
        }
    )
    level_styles = coloredlogs.DEFAULT_LEVEL_STYLES
    level_styles.update(
        {
            "info": {"color": "magenta"},
        }
    )
    fmt = "%(asctime)s %(programname)s@%(hostname)s %(levelname)-8s %(message)s"
    coloredlogs.install(
        fmt=fmt,
        level_styles=level_styles,
        field_styles=field_styles,
        level=arguments.log_level,
        logger=this,
        programname=name,
    )
    logging.captureWarnings(True)
    return this


def get_parser():
    """argument parser"""
    parser = configargparse.ArgParser(
        prog="pve-cloud-init-creator",
        auto_env_var_prefix="PVECIC_",
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(
        "--vm", help="create: vm name to create, destroy: vm id to destroy"
    )
    parser.add_argument(
        "--config", help="config file path", required=True
    ).complete = shtab.FILE
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument(
        "--log-level",
        type=str,
        choices=[
            "SPAM",
            "DEBUG",
            "VERBOSE",
            "INFO",
            "NOTICE",
            "WARNING",
            "SUCCESS",
            "ERROR",
            "CRITICAL",
        ],
        default="warning",
    )
    subparsers = parser.add_subparsers(
        dest="action",
        help="action",
    )
    subparsers.required = True
    subparsers.add_parser("create", help="create")
    subparsers.add_parser("destroy", help="destroy")
    return parser


def create_snippet_from_scratch(
    config: DefaultMunch,
    logger: logging.Logger,
):
    """Create cloud-init template from scratch"""
    cloud_init = yaml_safe_dump(DEFAULT_USER_DATA, default_flow_style=False)
    upload = io.BytesIO(f"#cloud-config\n{cloud_init}".encode())
    logger.debug("#cloud-config\n%s", cloud_init)
    try:
        res = requests.put(
            f"{config.snippets.endpoint}/snippets/{config.vm.id}.cfg",
            data=upload.getvalue(),
            timeout=60,
            verify_ssl=config.snippets.verify_ssl,
        )
        res.raise_for_status()
    # pylint: disable=broad-except
    except Exception as err:
        logger.error(err)
        sys_exit(1)


def create_snippet_from_file(
    config: DefaultMunch,
    logger: logging.Logger,
):
    """upload cloud-init snippet"""
    try:
        cloud_init = yaml_safe_dump(
            parse_config(
                config.vm.userdata,
                default_value=None,
                tag="!TAG",
            ),
            default_flow_style=False,
        )
    except FileNotFoundError as err:
        logger.error(err)
        sys_exit(1)
    upload = io.BytesIO(f"#cloud-config\n{cloud_init}".encode())
    logger.info("uploading cloud-init snippet")
    logger.debug("#cloud-config\n%s", cloud_init)
    try:
        res = requests.put(
            f"{config.snippets.endpoint}/snippets/{config.vm.id}.cfg",
            data=upload.getvalue(),
            timeout=60,
            verify=config.snippets.verify_ssl,
        )
        res.raise_for_status()
    # pylint: disable=broad-except
    except Exception as err:
        logger.error(err)
        sys_exit(1)


def destroy_snippet(
    config: DefaultMunch,
    arguments: configargparse.Namespace,
    logger: logging.Logger,
):
    """destroy existing snippets"""
    exists = False
    try:
        res = requests.head(
            f"{config.snippets.endpoint}/snippets/{arguments.vm}.cfg",
            timeout=60,
            verify=config.snippets.verify_ssl,
        )
        if res.status_code == 200:
            exists = True
    # pylint: disable=broad-except
    except Exception as err:
        logger.error(err)
        sys_exit(1)
    if exists:
        try:
            res = requests.delete(
                f"{config.snippets.endpoint}/snippets/{arguments.vm}.cfg",
                timeout=60,
                verify=config.snippets.verify_ssl,
            )
            res.raise_for_status()
        # pylint: disable=broad-except
        except Exception as err:
            logger.error(err)
            sys_exit(1)


def proxmoxclient(config: DefaultMunch):
    """return ProxmoxAPI"""
    return ProxmoxAPI(
        config.pve.host,
        port=config.pve.port,
        user=config.pve.user,
        password=config.pve.password,
        token_name=config.pve.token_name,
        token_value=config.pve.token_value,
        verify_ssl=config.pve.verify_ssl,
    )


# pylint: disable=too-many-locals
def create_vm(
    config: DefaultMunch,
    arguments: configargparse.Namespace,
    extra_arguments: configargparse.Namespace,
    logger: logging.Logger,
):
    """Create VM from cloud-init image"""
    proxmox = proxmoxclient(config=config)
    newid = proxmox.cluster.nextid.get()
    config.vm.update({"id": newid})
    logger.info(f"ID={newid}")
    node = proxmox.nodes(config.pve.node)
    args = {
        "node": config.pve.node,
        "vmid": newid,
        "memory": config.vm.memory.size,
        "cores": config.vm.cpu.cores,
        "cpu": config.vm.cpu.type,
        "ipconfig0": config.vm.ipconfig or "ip=dhcp",
        "net0": config.vm.netconfig or "virtio,bridge=vmbr0",
        "ostype": config.vm.ostype or "l26",
        "boot": "order=scsi0",
        "serial0": "socket",
        "vga": "serial0",
        "scsihw": config.vm.scsihw or "virtio-scsi-single",
        "ide2": f"{config.pve.storage}:cloudinit",
        "name": arguments.vm,
        "pool": config.pve.pool,
    }
    if config.vm.ssh_key:
        DEFAULT_USER_DATA.update(
            {
                "hostname": arguments.vm,
                "ssh_authorized_keys": [config.vm.ssh_key],
            }
        )
        create_snippet_from_scratch(
            config=config,
            logger=logger,
        )
        args.update({"sshkeys": quote(config.vm.ssh_key, safe="")})
    elif config.vm.userdata:
        create_snippet_from_file(
            config=config,
            logger=logger,
        )
        args.update(
            {"cicustom": f"user={config.snippets.storage}:snippets/{arguments.vm}.cfg"}
        )
    for extra_argument in extra_arguments:
        try:
            extra_argument = extra_argument.split("=")
            args.update({extra_argument[0]: "=".join(extra_argument[1:])})
        except ValueError:
            logger.warning(f"ignoring malformed argument {extra_argument}")
    tasks = {
        "create vm": (node.qemu.post, args),
        "import disk": (
            node.qemu(newid).config.post,
            {"scsi0": f"{config.pve.storage}:0,import-from={config.vm.image}"},
        ),
        "resize disk": (
            node.qemu(newid).resize.put,
            {"disk": "scsi0", "size": config.vm.storage.size},
        ),
        "start vm": (node.qemu(newid).status.start.post, {}),
    }
    for task, data in tasks.items():
        logger.info(task)
        method, args = data
        logger.debug(args)
        try:
            upid = method(**args)
            time.sleep(5)
            status = "running"
            while status == "running":
                try:
                    status = node.tasks(upid).status.get().get("status")
                except RequestsConnectionError as err:
                    logger.debug("%s while checking task %s status", err, upid)
                time.sleep(5)
            time.sleep(5)
        except ResourceException as err:
            logger.error(err)
            sys_exit(1)


# pylint: disable=unused-argument
def destroy_vm(
    config: DefaultMunch,
    arguments: configargparse.Namespace,
    logger: logging.Logger,
    **kwargs,
):
    """destroy vm"""
    proxmox = proxmoxclient(config=config)
    node = proxmox.nodes(config.pve.node)
    try:
        logger.info("removing cloud-init snippet if exists")
        destroy_snippet(
            config=config,
            arguments=arguments,
            logger=logger,
        )
        logger.info("shutting down vm")
        node.qemu(arguments.vm).status.shutdown.post()
        time.sleep(5)
        while node.qemu(arguments.vm).status.current.get().get("status") == "running":
            time.sleep(5)
        time.sleep(5)
        logger.info("deleting vm")
        node.qemu(arguments.vm).delete()
    except ResourceException as err:
        logger.error(err)
        sys_exit(1)


def main():
    """main function"""
    parser = get_parser()
    arguments, extra_arguments = parser.parse_known_args()
    logger = get_logger(
        name="pve-cloud-init-creator",
        arguments=arguments,
    )
    try:
        _config = clean_dict(
            parse_config(arguments.config, default_value=None, tag="!TAG")
        )
        logger.debug("config:\n%s", yaml_safe_dump(_config))
        validate(
            instance=_config,
            schema=CONFIG_SCHEMA,
        )
        config = DefaultMunch.fromDict(_config)
    except FileNotFoundError:
        logger.error("config file %s not found", arguments.config)
        sys_exit(1)
    except ValidationError as err:
        path = err.path
        if err.parent:
            path = err.parent.path
        path = ":".join(list(path))
        msg = f"{path}: {err.message}"
        if err.validator == "oneOf":
            msg = f"{path}: expected only one of {err.validator_value}"
        logger.error("%s", msg)
        sys_exit(1)
    action_map = {
        "create": create_vm,
        "destroy": destroy_vm,
    }
    action_map.get(arguments.action)(
        config=config,
        arguments=arguments,
        extra_arguments=extra_arguments,
        logger=logger,
    )


if __name__ == "__main__":
    main()
