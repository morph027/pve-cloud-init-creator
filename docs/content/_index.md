# Proxmox VE Cloud-Init Creator (pvecic)

## Scope

Proxmox VE supports [Cloud Init](https://pve.proxmox.com/wiki/Cloud-Init_Support) so one can easily bootstrap VM's without PXE/Kickstart/Preseed/...

This project provides the following tools to do so:

* client binary to interact with PVE cluster nodes
* pvecic-snippets-endpoint: snippets API endpoint
* pvecic-image-mirror: serve local mirror of Ubuntu cloud images
* pvecic-image-sync: sync local mirror of Ubuntu cloud images

Standing on the shoulders of giants, tools being used:

* [proxmoxer](https://github.com/proxmoxer/proxmoxer/): PVE API communication
* [rclone](https://rclone.org/): Simple HTTP WEBDAV server for snippets (until snippet upload is possible through API itself, see [feature request in bugtracker](https://bugzilla.proxmox.com/show_bug.cgi?id=2208)) and cloud-image mirror
* [Nuitka](https://nuitka.net/): pre-compiling python code to single binary
* [Packer (optional)](https://www.packer.io/): customize the cloud images

Motivation was to setup dynamic Gitlab runners for a multiarch docker image build [here](https://gitlab.com/morph027/signal-web-gateway/pipelines/134306488).
