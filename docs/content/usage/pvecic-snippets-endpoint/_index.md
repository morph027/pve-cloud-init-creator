+++
title = "Snippets endpoint"
weight = 2
+++

## Authentication

Authentication will be handled by basic auth from file `/etc/pvecic-snippets-endpoint/htpasswd` and can be managed using the [htpasswd](https://httpd.apache.org/docs/current/programs/htpasswd.html) command.

{{% notice note %}}
The service will not start without authentication in place.
{{% /notice %}}

### create user/password

```bash
sudo -u pvecic -H htpasswd -c /etc/pvecic-snippets-endpoint/htpasswd pvecic
```

## Configuration values

Options can be set in `/etc/default/pvecic-snippets-endpoint`. If empty or does not exist, the default values from `/usr/share/pvecic/snippets-endpoint.env` will be applied.

| Value | Comment |
|-|-|
| **`ADDR`** | listen address of snippets endpoint, e.g. `0.0.0.0:8005` |
| **`DIR`** | pve snippets storage directory to serve |
| **`PARAMS`** | other [rclone serve webdav](https://rclone.org/commands/rclone_serve_webdav/) parameters |

## Configuration examples

### Shared storage

```
DIR="/mnt/pve/your-shared-storage-identifier/snippets"
```

### SSL certs

Instead of the snakeoil certs, you could also use your pve cluster certs.

```
PARAMS="--cert /etc/pve/pve-root-ca.pem --key /etc/pve/pve-www.key"
```
