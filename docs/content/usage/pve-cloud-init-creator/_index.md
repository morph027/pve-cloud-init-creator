+++
title = "Cloud-Init Creator"
weight = 5
+++

Either create a configuration file (see `config.template.yaml`) or use the template itself and set options via environment variables.

```
export PVE_HOST=proxmox.example.com PVE_PORT=443 PVE_USER=user@pve PVE_NODE=node SNIPPETS_ENDPOINT=https://user:password@proxmox.example.com:8005
pve-cloud-init-creator --config config.template.yaml --vm my-dummy-vm create
```

This command will print a VMID which you can use to destroy the VM later:

```
export PVE_HOST=proxmox.example.com PVE_PORT=443 PVE_USER=user@pve PVE_NODE=node SNIPPETS_ENDPOINT=https://user:password@proxmox.example.com:8005
pve-cloud-init-creator --<VMID> destroy
```

## Example with custom cloud-init data

Create a cloud-init userdata file and add it to the config file (either hardcoded or using `${VM_CLOUDINIT_USERDATA}`).

{{% notice tip %}}
You can use environment variables too, e.g:
- `!TAG ${ENV_VAR}`
- `!TAG tag:yaml.org,2002:int ${ENV_VAR:1024}`
- `!TAG tag:yaml.org,2002:bool ${ENV_VAR:false}`

For more see https://pypi.org/project/pyaml-env/
{{% /notice %}}

{{% notice note %}}
`#cloud-config` header will be added automatically.
{{% /notice %}}

```yaml
---
# yamllint disable rule:line-length
hostname: !TAG ${HOST}
manage_etc_hosts: true
ssh_authorized_keys:
  - ssh-rsa ...
chpasswd:
  expire: false
users:
  - default
package_update: true
packages:
  - docker.io
  - qemu-guest-agent
write_files:
  - content: |
      #!/bin/bash
      echo "I'm a script to do something fancy on first boot"
    path: /var/lib/cloud/scripts/per-once/01-fancy-script.sh
    permissions: '0755'
runcmd:
  - systemctl start qemu-guest-agent.service
  # apply hostname w/o reboot
  - systemctl restart systemd-networkd.service
```

## Extra

### Extra API parameters

You can pass any POST parameter from [PVE API Docs](https://pve.proxmox.com/pve-docs/api-viewer/index.html#/nodes/{node}/qemu/{vmid}/config) in the form of `<name>=<value>`

```
pve-cloud-init-creator --config config.template.yaml --vm my-dummy-vm create audio0='device=intel-hda' agent='enabled=true,fstrim_cloned_disks=true,type=virtio'
```

### Custom Images

If you need to modify the cloud-init images, you can use packer like described [here](https://discourse.ubuntu.com/t/building-multipass-images-with-packer/12361)
