+++
title = "Image sync"
weight = 3
+++

{{% notice note %}}
Optional
{{% /notice %}}

## Configuration values

Options can be set in `/etc/default/pvecic-image-sync`. If empty or does not exist, the default values from `/usr/share/pvecic/image-sync.env` will be applied.

| Value | Comment |
|-|-|
| **`DIR`** | directory to sync cloud images to - if shared storage, just run the sync on only a single node |
| **`PARAMS`** | other [rclone sync](https://rclone.org/commands/rclone_sync/) parameters |

## Enable daily sync

```bash
systemctl enable --now pvecic-image-sync.timer
```

## Configuration examples

### sync multiple codenames

```
PARAMS="-v --delete-excluded --include {focal,jammy,noble}/release/*-minimal-cloudimg-amd64.img --include {focal,jammy,noble}/release/*SUMS* --http-url http://cloud-images.ubuntu.com/minimal/releases"
```
