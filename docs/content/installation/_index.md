+++
title = "Installation"
weight = 1
+++

## Client only binary

Download latest binary from [here](https://gitlab.com/morph027/pve-cloud-init-creator/-/jobs/artifacts/master/raw/pve-cloud-init-creator?job=pve-cloud-init-creator:package) (does not work on alpine linux, requires glibc)

## Client only Docker

Just use the container image `registry.gitlab.com/morph027/pve-cloud-init-creator:latest`

## Repository

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-pve-cloud-init-creator.asc https://morph027.gitlab.io/pve-cloud-init-creator/gpg.key
```

### Add repo to apt

```bash
echo "deb https://morph027.gitlab.io/pve-cloud-init-creator pve-cloud-init-creator main" | sudo tee /etc/apt/sources.list.d/morph027-pve-cloud-init-creator.list
sudo apt-get update
```

## Packages

* `pve-cloud-init-creator`: client binary to interact with PVE cluster nodes using proxmoxer
* `pvecic-snippets-endpoint`: snippets API endpoint using rclone
* `pvecic-image-mirror`: serve local mirror of Ubuntu cloud images using rclone
* `pvecic-image-sync`: sync local mirror of Ubuntu cloud images using rclone
